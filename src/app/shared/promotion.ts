export class Promotion {

    id: String;
    name : String;
    image: String;
    label: string;
    price: string;
    featured: boolean;
    description: string;

}