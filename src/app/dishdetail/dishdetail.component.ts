import { Component, OnInit, Input, ViewChild, Inject } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Location } from '@angular/common';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { switchMap } from 'rxjs/operators';
import { Comment } from '../shared/comment';
import { visibility, flyInOut, expand } from '../animations/app.animation';


@Component({
    selector: 'app-dishdetail',
    templateUrl: './dishdetail.component.html',
    styleUrls: ['./dishdetail.component.scss'],
    host: {
      '[@flyInOut]' : 'true',
      'style' : 'display : block'
    },
    animations: [
      flyInOut(),
      visibility(),
      expand()
    ]

})
export class DishdetailComponent implements OnInit {
    id: string;

    dish: Dish;
    errMess : string;
    dishIds: string[];
    prev: string;
    next: string;
    comment: Comment;
    commentsForm : FormGroup;
    dishCopy: Dish;
    visibility = 'shown';
    
    //dish = Dish;
    constructor(private dishService: DishService, private location: Location, private route: ActivatedRoute, private fb : FormBuilder, @Inject('BaseURL') private BaseURL) { 
        this.createForm();
    }

    @ViewChild('cform', {static: true}) commentsFormDirective;

    createForm(){
        this.commentsForm = this.fb.group({
          author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
          comment: ['', [Validators.required, Validators.minLength(2)]],
          rating: 5,
          date : ''
        });

        this.commentsForm.valueChanges.subscribe(data => this.onValueChanged(data));

        this.onValueChanged(); // reset form validation msgs
    }

    onValueChanged(data?: any) {
        if(!this.commentsForm) { return; }
        const form = this.commentsForm;
        for (const field in this.formErrors) {
          if(this.formErrors.hasOwnProperty(field)) {
            // clear previous error messages
            this.formErrors[field] = '';
            const control = form.get(field);
            if(control && control.dirty && !control.valid) {
              const messages = this.validationMessages[field];
              for(const key in control.errors) {
                if(control.errors.hasOwnProperty(key)) {
                  this.formErrors[field] += messages[key] + ' ';
                }
              }
            }
          }
        }
      }

        formErrors = {
            'author': '',
              'comment': ''
          };

          validationMessages = {
            'author': {
              'required' : 'Author Name is Required.',
              'minlength' : 'Author Name must be at least 2 characters long ',
              'maxlength' : 'Author Name cannot be more than 25 characters'
            },
            'comment': {
              'required' : 'Comment is Required.',
              'minlength' : 'Comments must be at least 2 characters'
            }
          };

    ngOnInit() {
        this.dishService.getDishIds().subscribe((dishIds) => this.dishIds = dishIds);
        this.route.params
        .pipe(switchMap((params: Params) => { this.visibility ='hidden'; return this.dishService.getDish(params['id']);}))
        .subscribe(dish => { this.dish = dish; this.dishCopy = dish; this.setPrevNext(dish.id); this.visibility ='shown';}
        , errmess => this.errMess = <any>errmess);
    }

    setPrevNext(dishId: string) {
        const index = this.dishIds.indexOf(dishId);
        this.prev = this.dishIds[(this.dishIds.length + index - 1 )  % this.dishIds.length];
        this.next = this.dishIds[(this.dishIds.length + index + 1 )  % this.dishIds.length];
    }

    goBack(): void {
        this.location.back();
    }

    onSubmit(){

      this.comment = this.commentsForm.value;
    this.comment.date = new Date().toISOString();
    this.dishCopy.comments.push(this.comment);
    this.dishService.putDish(this.dishCopy).subscribe(dish => {
      this.dish = dish; this.dishCopy = dish;
    },
  errmess => {this.dish = null; this.dishCopy = null; this.errMess = <any>errmess;});
    console.log(this.comment);
        this.commentsForm.reset({
          author: '',
          comment: '',
          rating: 5,
          date : ''
        });
      }

}
