import { pipe } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';
import { flyInOut, expand } from '../animations/app.animation';
import { FeedbackService } from '../services/feedback.service';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display : block'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})

export class ContactComponent implements OnInit {

  feedbackForm: FormGroup;
  feedback: Feedback;
  feedbackCopy: Feedback;
  feedbackSaved: Feedback;
  contactType = ContactType;
  errMess: string;
  spinner: boolean = false;
  response: boolean = false;

  @ViewChild('fform', { static: true }) feedbackFormDirective;

  formErrors = {
    'firstname': '',
    'lastname': '',
    'telnum': '',
    'email': '',
  };

  validationMessages = {
    'firstname': {
      'required': 'First Name is Required,',
      'minlength': 'First Name must be at least 2 characters',
      'maxlength': 'First Name cannot be more than 25 characters'
    },
    'lastname': {
      'required': 'Last Name is Required,',
      'minlength': 'Last Name must be at least 2 characters',
      'maxlength': 'Last Name cannot be more than 25 characters'
    },
    'telnum': {
      'required': 'Tel number is Required,',
      'pattern': 'Tel number must contain only numbers'
    },
    'email': {
      'required': 'Email is Required,',
      'pattern': 'Email is in invalid format'
    }
  };

  constructor(private fb: FormBuilder, private feedbackService: FeedbackService) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      telnum: [0, [Validators.required, Validators.pattern]],
      email: ['', [Validators.required, Validators.email]],
      agree: false,
      contacttype: 'None',
      message: ''
    });

    this.feedbackForm.valueChanges.subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // reset form validation msgs
  }
  onValueChanged(data?: any) {
    if (!this.feedbackForm) { return; }
    const form = this.feedbackForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error messages
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  onSubmit() {
    this.spinner = true;
    this.feedback = this.feedbackForm.value;
    console.log(this.feedback);
    console.log(this.spinner);
    this.feedbackCopy = this.feedback;
    this.feedbackService.submitFeedback(this.feedbackCopy).pipe(delay(3000)).subscribe(feedback => {
      this.feedback = feedback; this.feedbackCopy = feedback; this.feedbackSaved = feedback; this.response = true;
      setTimeout(() => {
        this.response = false;
        this.spinner = false;
      }, 5000);
    },
      errmess => { this.feedback = null; this.feedbackCopy = null; this.errMess = <any>errmess; 
        setTimeout(() => {
          this.response = false;
          this.spinner = false;
          this.errMess = null;
        }, 5000);});
    this.feedbackForm.reset({
      firstname: '',
      lastname: '',
      telnum: 0,
      email: '',
      agree: false,
      contacttype: 'None',
      message: ''
    });
    this.feedbackFormDirective.resetForm();
    //this.spinner = false;
    console.log(this.spinner);
  }
}
